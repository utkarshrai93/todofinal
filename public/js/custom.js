(function blink() {
    $('.blink_me').fadeOut(50).fadeIn(500, blink);
})();

$('.modalClose').click(function () {
    $('#app-layout').css('padding-right','-30px !important')
})
$('.editForm').click(function (e) {
    $('[name]').val($(this).attr('data_value2'));
    $(slug).val($(this).attr('data_value'));
})

$(document).ready(function () {
    $('.alert-info').on('load','td.warning input',function () {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted!", "success");
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    });

});
