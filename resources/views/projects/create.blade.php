@extends('layouts.app')

@section('content')
    <div class="row" style="background-color:white">
        <div class="box-header with-border" style="margin-top: 10px;">
            <h3 class="box-title "><i class="glyphicon glyphicon-pencil" style="font-size: 13px"></i> Create Project</h3>
        </div>

    {!! Form::model(new App\Project, ['route' => ['projects.store']]) !!}
    @include('projects/partials/_form', ['submit_text' => 'Create Project')
    {!! Form::close() !!}
    </div>
@endsection
