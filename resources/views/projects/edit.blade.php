@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="box-header with-border" style="margin-top: 10px;">
            <h3 class="box-title "><i class="glyphicon glyphicon-pencil" style="font-size: 13px"></i> Edit Project</h3>
    {!! Form::model($project, ['method' => 'PATCH', 'route' => ['projects.update', $project->slug]]) !!}
    @include('projects/partials/_form', ['submit_text' => 'Edit Project'])
    {!! Form::close() !!}
    </div>
@endsection