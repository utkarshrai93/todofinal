@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="padding-top: 20px">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Projects</h3>
                    <a style="font-size: 12px" class="pull-right btn btn-primary" data-toggle="modal" data-target="#myModal">Add Project</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if ( !$projects->count() )
                        You have no projects
                    @else
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Project Name</th>
                            <th>Creation Date</th>
                            <th>Updation Date</th>
                            <th style="width: 30px">Edit</th>
                            <th style="width: 40px">Delete</th>
                        </tr>
                        @foreach( $projects as $key=>$project )
                        <tr>
                            {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('projects.destroy', $project->slug))) !!}
                            <td><?php echo $key+1?>.</td>
                            <td><a  href="{{ route('projects.show', $project->slug) }}">{{ $project->name }}</a></td>
                            <td>{{ $project->created_at }}</td>
                            <td>{{ $project->updated_at }}</td>
                            <td>
                                {{--<button type="button" style="font-size: 12px"  class="pull-right btn btn-info glyphicon glyphicon-pencil editForm" data_value="{{ $project->slug }}" data_value2="{{ $project->name }}" data-toggle="modal" data-target="#myModal2"></button>--}}
                                {!! link_to_route('projects.edit', '', array($project->slug), array('class' => 'btn btn-info glyphicon glyphicon-pencil')) !!}
                            </td>
                            <td>{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}</td>
                            {!! Form::close() !!}
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="row">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Project</h4>
                        </div>
                        {!! Form::model(new App\Project, ['route' => ['projects.store']]) !!}
                        <div class="modal-body">
                            <table class="table table-responsive text-center" style="">
                                <tr class="form-group">
                                    <td> {!! Form::label('name', 'Name:') !!}</td>
                                    <td>{!! Form::text('name',null,['class'=>'form-control']) !!}</td>
                                </tr>
                                <tr class="form-group" >
                                    <td>{!! Form::label('slug', 'Slug:') !!}</td>
                                    <td>{!! Form::text('slug',null,['class'=>'form-control']) !!}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                            {!! Form::submit('Create Project', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
       {{-- <div class="row">
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Project</h4>
                        </div>

                        {!! Form::model($project, ['method' => 'PATCH', 'route' => ['projects.update', $project->slug]]) !!}
                        <div class="modal-body">
                            <table class="table table-responsive text-center" style="">
                                <tr class="form-group">
                                    <td> {!! Form::label('name', 'Name:') !!}</td>
                                    <td>{!! Form::text('name') !!}</td>
                                </tr>
                                <tr class="form-group" >
                                    <td>{!! Form::label('slug', 'Slug:') !!}</td>
                                    <td>{!! Form::text('slug') !!}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                            {!! Form::submit('Edit Project', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>--}}
    </div>


@endsection