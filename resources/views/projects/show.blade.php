@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="padding-top: 20px">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Project : </b>{{ $project->name }}</h3>
                    <a style="font-size: 12px" class="pull-right btn btn-primary" data-toggle="modal" data-target="#myModal">Add Task</a>
                </div>
                @if ( !$project->tasks->count() )
                    Your project has no tasks.
                @else
                <div class="box-body">
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Tasks</th>
                            <th>Creation Date</th>
                            <th>Updation Date</th>
                            <th style="width: 30px">Edit</th>
                            <th style="width: 40px">Delete</th>
                        </tr>
                        @foreach( $project->tasks as $key=>$task )
                            <tr>
                                <td>
                                    {{$key+1}}
                                </td>
                                <td>
                                    {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('projects.tasks.destroy', $project->slug, $task->slug))) !!}
                                    <a href="{{ route('projects.tasks.show', [$project->slug, $task->slug]) }}">{{ $task->name }}</a>
                                </td>
                                <td>{{ $project->created_at }}</td>
                                <td>{{ $project->updated_at }}</td>
                                <td>
                                    {!! link_to_route('projects.tasks.edit', '', array($project->slug, $task->slug), array('class' => 'btn btn-info glyphicon glyphicon-pencil')) !!}
                                </td>
                                <td>
                                    {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                </td>
                                {!! Form::close() !!}
                            </tr>
                        @endforeach
                    </table>
                 </div>
                @endif
        </div>
            <div class="row">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Create Task for {{$project->name }}</h4>
                            </div>
                            {!! Form::model(new App\Task, ['route' => ['projects.tasks.store', $project->slug], 'class'=>'']) !!}                            <div class="modal-body">
                                <table class="table table-responsive" style="">
                                    <tr class="form-group">
                                        <td>
                                            {!! Form::label('name', 'Name:') !!}
                                        </td>
                                        <td>
                                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                                        </td>
                                    </tr>

                                    <tr class="form-group">
                                        <td>
                                            {!! Form::label('slug', 'Slug:') !!}
                                        </td>
                                        <td>
                                            {!! Form::text('slug',null,['class'=>'form-control']) !!}
                                        </td>
                                    </tr>

                                    <tr class="form-group">
                                        <td>
                                            {!! Form::label('completed', 'Completed:') !!}
                                        </td>
                                        <td>
                                            {!! Form::checkbox('completed') !!}
                                        </td>
                                    </tr>

                                    <tr class="form-group">
                                        <td>
                                            {!! Form::label('description', 'Description:') !!}
                                        </td>
                                        <td>
                                            {!! Form::textarea('description',null,['class'=>'form-control']) !!}
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="modal-footer">
                                <a  href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                                {!! Form::submit('Create Task',['class'=>'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection