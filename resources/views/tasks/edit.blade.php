@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="box-header with-border" style="margin-top: 10px;">
            <h3 class="box-title "><i class="glyphicon glyphicon-pencil" style="font-size: 13px"></i> Edit Task : {{ $task->name }}</h3>
        </div>
        {!! Form::model($task, ['method' => 'PATCH', 'route' => ['projects.tasks.update', $project->slug, $task->slug]]) !!}
        @include('tasks/partials/_form', ['submit_text' => 'Edit Task'])
        {!! Form::close() !!}
    </div>
@endsection