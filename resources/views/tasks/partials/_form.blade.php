<div class="box-body">
    <!-- general form elements -->
    <table class="table table-responsive" style="">
        <tr class="form-group">
            <td>
                {!! Form::label('name', 'Name:') !!}
            </td>
            <td>
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </td>
        </tr>

        <tr class="form-group">
            <td>
                {!! Form::label('slug', 'Slug:') !!}
            </td>
            <td>
                {!! Form::text('slug',null,['class'=>'form-control']) !!}
            </td>
        </tr>

        <tr class="form-group">
            <td>
                {!! Form::label('completed', 'Completed:') !!}
            </td>
            <td>
                {!! Form::checkbox('completed') !!}
            </td>
        </tr>

        <tr class="form-group">
            <td>
                {!! Form::label('description', 'Description:') !!}
            </td>
            <td>
                {!! Form::textarea('description',null,['class'=>'form-control']) !!}
            </td>
        </tr>

        <tr class="form-group">
            <td>
                {!! Form::submit($submit_text,['class'=>'btn primary','style'=>'margin-top:20px']) !!}
                <?php $var="/projects/".$project->slug; ?>
                {{--{!! link_to_route('projects.index', 'Cancel',null, array('class' => 'btn btn-danger','style'=>'margin-top:20px')) !!}</td>--}}
                <a class="btn btn-danger" style="margin-top:20px" href="{{$var}}">Cancel</a>
            </td>
        </tr>
    </table>
</div>