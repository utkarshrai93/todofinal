@extends('layouts.app')

@section('content')



    <div class="row">
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class=""></i>

                    <h3 class="box-title">
                            {!! link_to_route('projects.show', $project->name, [$project->slug]) !!} - {{ $task->name }}
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <blockquote>
                        <p>
                            {{ $task->description }}
                        </p>
                    </blockquote>
                </div>
                <!-- /.box-body -->
            </div>
        {!! link_to_route('projects.show', ' Back', array($project->slug, $task->slug), array('class' => 'btn btn-info fa fa-arrow-left')) !!}
            <!-- /.box -->
        </div>
@endsection