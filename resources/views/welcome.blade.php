@extends('layouts.app')

@section('content')
<div class="container text-center">
    <div class="row text-center" style="margin: auto;">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Welcome! to the Todos Application
                </div>
                <div class="panel-body">
                    Manage your projects and tasks.
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
